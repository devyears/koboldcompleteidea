# KoboldComplete Pycharm Extension
## Description
 - This is an extension that allows users to generate code/text using KoboldCPP.
## Available commands
 - `KoboldComplete: Autocomplete`
   - Sends code (500 symbols above the cursor) to KoboldCPP
   - To use it
     - Press ctrl+alt+g
     - or press shift twice and select action KoboldComplete: Autocomplete
 - `KoboldComplete: Ask Question`
   - Select text in IDE press shift twice and select action KoboldComplete: Ask Question
   - Result will appear below or error will be displayed
## Settings
 - Settings/Tools/KoboldComplete Settings
   - host: KoboldCpp address 
## Installation
 - Download repository
 - Open it with IntelliJ IDEA
 - Press shift twice and select Execute Gradle Task
 - Select `gradle buildPlugin`
 - Open PyCharm
 - Press shift twice
 - Select Install Plugin from disk
 - Find plugin in `{repository_name}/distributions/koboldcomplete-version.jar`
// Copyright 2000-2022 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
package com.devyears.koboldcomplete

import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBTextField
import com.intellij.util.ui.FormBuilder
import javax.swing.JComponent
import javax.swing.JPanel

/**
 * Supports creating and managing a [JPanel] for the Settings Dialog.
 */
class AppSettingsComponent {
    val panel: JPanel
    private val myHostText = JBTextField()
    private val myUserNameText = JBTextField()
    private val myIdeaUserStatus = JBCheckBox("Do you use IntelliJ IDEA? ")

    init {
        panel = FormBuilder.createFormBuilder()
//                .addLabeledComponent(JBLabel("Enter user name: "), myUserNameText, 1, false)
                .addLabeledComponent(JBLabel("KoboldCPP host address: "), myHostText, 1, false)
//                .addComponent(myIdeaUserStatus, 1)
                .addComponentFillVertically(JPanel(), 0)
                .panel
    }

    val preferredFocusedComponent: JComponent
        get() = myHostText

    var hostText: String
        get() = myHostText.getText()
        set(newText) {
            myHostText.setText(newText)
        }

    var userNameText: String
        get() = myUserNameText.getText()
        set(newText) {
            myUserNameText.setText(newText)
        }
    var ideaUserStatus: Boolean
        get() = myIdeaUserStatus.isSelected
        set(newStatus) {
            myIdeaUserStatus.setSelected(newStatus)
        }
}

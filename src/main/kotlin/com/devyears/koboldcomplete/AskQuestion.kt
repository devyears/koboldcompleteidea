package com.devyears.koboldcomplete

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.json.responseJson
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.ui.Messages
import net.minidev.json.JSONObject

class AskQuestion : AnAction() {
    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }

    override fun actionPerformed(e: AnActionEvent) {
        // Get access to the editor and caret model. update() validated editor's existence.
        val project = e.getRequiredData(CommonDataKeys.PROJECT)
        val editor = e.getRequiredData(CommonDataKeys.EDITOR)
        val document = editor.document
        val caretModel = editor.caretModel
        // Getting the primary caret ensures we get the correct one of a possible many.
        val primaryCaret = caretModel.primaryCaret
        val start = primaryCaret.selectionEnd
        val host = AppSettingsState.instance.host

        val question = editor.selectionModel.selectedText
        val prompt = "<s>[INST]$question[/INST]"

        val body = JSONObject()
        body["prompt"] = prompt
        body["temperature"] = 0.5
        body["top_p"] = 0.9
        body["max_length"] = 512

        ReadAction.run<Exception> {
            val (request, response, result) = Fuel.post("$host/api/v1/generate").jsonBody(body.toString()).responseJson()
            result.fold({ data ->
                try {
                    val jsonObject = data.obj()
                    val text = jsonObject.getJSONArray("results").getJSONObject(0).getString("text")
                    WriteCommandAction.runWriteCommandAction(project) {
                        document.insertString(start, "\n$text")
                    }
                } catch (error: Exception) {
                    Messages.showInfoMessage("An error: ${error.message} happened", "KoboldComplete: Error Occurred")
                }
            }, { error ->
                Messages.showInfoMessage("An error of type ${error.exception} happened: ${error.message}", "KoboldComplete: Error Occurred")
            })
        }
    }

    override fun update(e: AnActionEvent) {
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)

        e.presentation.setEnabledAndVisible(project != null && editor != null && editor.selectionModel.hasSelection())
    }
}
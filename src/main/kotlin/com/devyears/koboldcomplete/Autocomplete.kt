package com.devyears.koboldcomplete

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.json.responseJson
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.util.TextRange
import net.minidev.json.JSONObject

class Autocomplete : AnAction() {
    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }

    override fun actionPerformed(e: AnActionEvent) {
        // Get access to the editor and caret model. update() validated editor's existence.
        val project = e.getRequiredData(CommonDataKeys.PROJECT)
        val editor = e.getRequiredData(CommonDataKeys.EDITOR)
        val document = editor.document
        val caretModel = editor.caretModel
        // Getting the primary caret ensures we get the correct one of a possible many.
        val primaryCaret = caretModel.primaryCaret
        val start = primaryCaret.selectionEnd
        val codeBufferMaxLength = 200
        var code = document.getText(TextRange(0, start))
        var codeBufferStart = code.length - codeBufferMaxLength
        if (codeBufferStart < 0) codeBufferStart = 0
        code = code.substring(codeBufferStart until code.length)
        val currentLanguage = "python"
        val host = AppSettingsState.instance.host
        val prompt = "<s>[INST]You are writing short code completion. Do not include descriptions.[/INST]```${currentLanguage}\n${code}"

        val body = JSONObject()
        body["prompt"] = prompt
        body["temperature"] = 0.5
        body["top_p"] = 0.9
        body["max_length"] = 512

        ReadAction.run<Exception> {
            val (request, response, result) = Fuel.post("$host/api/v1/generate").jsonBody(body.toString()).responseJson()
            result.fold({ data ->
                try {
                    val jsonObject = data.obj()
                    var text = jsonObject.getJSONArray("results").getJSONObject(0).getString("text")
                    // Replace markdown code delimiters
                    text = text.replace("```", "")
                    WriteCommandAction.runWriteCommandAction(project) {
                        document.insertString(start, text)
                    }
                } catch (error: Exception) {
                    Messages.showInfoMessage("An error: ${error.message} happened", "KoboldComplete: Error Occurred")
                }
            }, { error ->
                Messages.showInfoMessage("An error of type ${error.exception} happened: ${error.message}", "KoboldComplete: Error Occurred")
            })
        }
    }

    override fun update(e: AnActionEvent) {
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)

        e.presentation.setEnabledAndVisible(project != null && editor != null)
    }
}